class ApplicationController < ActionController::API
  include ActionController::HttpAuthentication::Token::ControllerMethods
  # protected methods for extend through the controllers

  # Add a before_action to authenticate all requests.
  # Move this to subclassed controllers if you only
  # want to authenticate certain methods.
  # before_action :authenticate

  protected
  # Authenticate the user with token based authentication
  def authenticate
    authenticate_token || render_unauthorized
  end

  def authenticate_token
    # authenticate_with_http_token do |token, options|
    #   # @current_user = User.find_by(apitoken: token)
    # end
    @current_user = User.find_by(apitoken: request.headers["apikey"])
    # render json: {:ms => @current_user}
  end

  def render_unauthorized(realm = "Application")
    self.headers["WWW-Authenticate"] = %(Token realm="#{realm.gsub(/"/, "")}")
    render json: {:error => "Bad credentials"}, status: :unauthorized
  end

  def serialize(active_record_request)
    return :data => active_record_request
  end

  def response_status
    status = {
      "ok" => 200,
      "created" => 201,
      "deleted" =>  204
    }
    return status
  end

  def loose_record
    render json: '{"error":"Resquested item does not found"}', status: 422
  end
end
