module Api
  module V1
    class SalesController < ApplicationController
      before_action :set_sale, only: [:show, :update, :destroy]
      rescue_from ActiveRecord::RecordNotFound, with: :loose_record
      before_action :authenticate

      # GET /sales
      def index
        @sales = Sale.where(user_id: @current_user.id).all
        render json: serialize(@sales.as_json({:include => [
          :product,
           :user => {:only => [:id, :name, :email, :phone_number]}
           ]}))
      end

      # GET /sales/1
      def show
        render json: serialize(@sale.as_json({:include => [
          :product,
           :user => {:only => [:id, :name, :email, :phone_number]}
           ]}))
      end

      # POST /sales
      def create
        @sale = Sale.new(sale_params)
        @applicant = User.find_by id: params[:user_id]
        if @sale.save
          @selected_sale = @sale.as_json({:include => [
            :product,
            :user => {:only => [:id, :name, :email, :phone_number]}
          ]})
          @owner = User.find_by id: @selected_sale['product']['user_id']
          NotifierMailer.sale_notify(@selected_sale, @owner, @applicant).deliver
          render json: serialize(@selected_sale), status: :created
        else
          render json: @sale.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /sales/1
      def update
        if @sale.update(sale_params)
          render json: serialize(@sale)
        else
          render json: @sale.errors, status: :unprocessable_entity
        end
      end

      # DELETE /sales/1
      def destroy
        @sale.destroy
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_sale
        @sale = Sale.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def sale_params
        params.require(:sale).permit(:user_id, :product_id, :note)
      end
    end
  end
end
