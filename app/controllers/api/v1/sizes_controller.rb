module Api
  module V1
    class SizesController < ApplicationController
      before_action :set_size, only: [:show, :update, :destroy]
      rescue_from ActiveRecord::RecordNotFound, with: :loose_record
      before_action :authenticate

      # GET /sizes
      def index
        @sizes = Size.all
        render json: serialize(@sizes.as_json({ :include => [:category]}))
      end

      # GET /sizes/1
      def show
        render json: serialize(@size.as_json({ :include => [:category]}))
      end

      # POST /sizes
      def create
        @size = Size.new(size_params)
        if @current_user.admin == true
          if @size.save
            render json: serialize(@size), status: :created
          else
            render json: @size.errors, status: :unprocessable_entity
          end
        else
          render json: {:error => "Unauthorized"}, status: :unauthorized
        end
      end

      # PATCH/PUT /sizes/1
      def update
        if @current_user.admin == true
          if @size.update(size_params)
            render json: serialize(@size), status: :ok
          else
            render json: @size.errors, status: :unprocessable_entity
          end
        else
          render json: {:error => "Unauthorized"}, status: :unauthorized
        end
      end

      # DELETE /sizes/1
      def destroy
        if @current_user.admin == true
          @size.destroy
        else
          render json: {:error => "Unauthorized"}, status: :unauthorized
        end
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_size
        @size = Size.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def size_params
        params.require(:size).permit(:category_id, :name)
      end
    end
  end
end
