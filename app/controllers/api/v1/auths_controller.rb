module Api
  module V1
    class AuthsController < ApplicationController
      # before_action :set_auth, only: [:show, :update, :destroy]

      def login
        @new_apitoken = User.call_keygen
        @user = User.find_by_email(params[:email])
        if @user.password == params[:password]
          @user['apitoken'] = @new_apitoken
          @user.save!
          render json: serialize(@user), status: :ok
        else
          render json: {:error => "Bad credentials"}, status: :unauthorized
        end
      end

      # login with facebook_id, fnd an existent data registre in the db

      def facebook_login
        @user = User.find_by facebook_id: params[:facebook_id]
        if @user.present?
          @new_apitoken = User.call_keygen
          @user['apitoken'] = @new_apitoken
          @user.save!
          render json: serialize(@user), status: :ok
        else
          render json: {:error => "Badcredentials"}, status: :unauthorized
        end
      end

      def logout
        @new_apitoken = User.call_keygen
        @user = User.find(params[:id])
        @user['apitoken'] = @new_apitoken
        @user.save!
      end

      def example
        render json: params, status: :ok
      end

    end
  end
end
