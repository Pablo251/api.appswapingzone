module Api
  module V1
    class CategoriesController < ApplicationController
      before_action :set_category, only: [:show, :update, :destroy]
      rescue_from ActiveRecord::RecordNotFound, with: :loose_record
      before_action :authenticate

      # GET /categories
      def index
        @categories = Category.all
        render json: serialize(@categories.as_json({ :include => [:size]}))
      end

      # GET /categories/1
      def show
        render json: serialize(@category.as_json({ :include => [:size]}))
      end

      # POST /categories
      def create
        if @current_user.admin == true
          @category = Category.new(category_params)
          if @category.save
            render json: serialize(@category), status: :created
          else
            render json: @category.errors, status: :unprocessable_entity
          end
        else
          render json: {:error => "Unauthorized"}, status: :unauthorized
        end
      end

      # PATCH/PUT /categories/1
      def update
        if @current_user.admin == true
          if @category.update(category_params)
            render json: serialize(@category), status: :ok
          else
            render json: @category.errors, status: :unprocessable_entity
          end
        else
          render json: {:error => "Unauthorized"}, status: :unauthorized
        end
      end

      # DELETE /categories/1
      def destroy
        if @current_user.admin == true
          @category.destroy
        else
          render json: {:error => "Unauthorized"}, status: :unauthorized
        end
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_category
        @category = Category.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def category_params
        params.require(:category).permit(:name)
      end
    end
  end
end
