module Api
  module V1
    class PicturesController < ApplicationController
      before_action :set_picture, only: [:show, :update, :destroy]
      rescue_from ActiveRecord::RecordNotFound, with: :loose_record
      before_action :authenticate

      # GET /pictures
      def index
        @pictures = Picture.all
        render json: serialize(@pictures.as_json({:include => [:products]}))
      end

      # GET /pictures/1
      def show
        render json: serialize(@picture.as_json({:include => [:products]}))
      end

      # POST /pictures
      def create
        @picture = Picture.new(picture_params)
        # render json: params[:productid]
        # render json: response_status["created"]
        # render json: {:message => @picture.id}
        # Begin a ActiveRecord crash prevention
        begin
          if @picture.save
            sql = "INSERT INTO public.pictures_products(product_id, picture_id) VALUES (#{params[:productid]}, #{@picture.id});"
            records_array = ActiveRecord::Base.connection.execute(sql)
            render json: serialize(@picture), status: :created
          else
            render json: @picture.errors, status: :unprocessable_entity
          end
        rescue ActiveRecord::InvalidForeignKey
          render json: '{"error":"Resquested product do not exist"}', status: 422
        end
      end

      # PATCH/PUT /pictures/1
      def update
        if @picture.update(picture_params)
          render json: serialize(@picture.as_json({:include => [:products]}))
        else
          render json: @picture.errors, status: :unprocessable_entity
        end
      end

      # DELETE /pictures/1
      def destroy
        @picture.destroy
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_picture
        @picture = Picture.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def picture_params
        params.require(:picture).permit(:alias, :path_url, :based)
      end
    end
  end
end
