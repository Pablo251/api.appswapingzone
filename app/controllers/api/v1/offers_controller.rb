module Api
  module V1
    class OffersController < ApplicationController
      before_action :set_offer, only: [:show, :update, :destroy]
      rescue_from ActiveRecord::RecordNotFound, with: :loose_record
      before_action :authenticate

      # GET /offers
      def index
        @offers = Offer.all
        begin
          if params[:applicant]
            @offers = offers_join_response("applicant", params[:id])
          elsif params[:owner]
            @offers = offers_join_response("owner", params[:id])
          end
          render json: serialize(@offers)
        rescue ActiveRecord::StatementInvalid
          render json: '{"error":"Param not provided"}', status: 422
        end
      end

      # GET /offers/1
      def show
        @selected_offer = offers_join_response("id", params[:id])
        render json: serialize(@selected_offer)
      end

      # GET /offers/1
      def accept_swap
        # *Join offer
        @selected_offer = offers_join_response("id", params[:id])
        # *Select offered products
        @owner_product = Product.find_by id: @selected_offer[0]['owner_product']
        @applicant_product = Product.find_by id: @selected_offer[0]['applicant_product']
        # *Swap!
        @owner_product['user_id'] = @selected_offer[0]['applicant']
        @applicant_product['user_id'] = @selected_offer[0]['owner']
        # render json: {:error => @applicant_product[0]}
        # *Save the Swap
        if @applicant_product.save
          if @owner_product.save
            # *SQL query to delete all the offers for the owner_product
            sql_delete = "DELETE FROM public.offers
            WHERE owner_product=#{@selected_offer[0]['owner_product']};"
            delete_result = ActiveRecord::Base.connection.execute(sql_delete)
            render json: {:message => "Offer swap"}
          else
            render json: {:error => "Offer can not be processed, please try latter"}
          end
        else
          render json: {:error => "Offer can not be processed, please try latter"}
        end
      end

      # POST /offers
      def create
        @offer = Offer.new(offer_params)
        # render json: @offer, status: :created
        if @offer.save
          @selected_offer = offers_join_response("id", @offer.id)
          NotifierMailer.offer_notify(@selected_offer[0]).deliver
          render json: serialize(@selected_offer), status: :created
          # render json: {:ms => @selected_offer[0]['owner_email']}
        else
          render json: @offer.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /offers/1
      def update
        if @offer.update(offer_params)
          render json: serialize(@offer)
        else
          render json: @offer.errors, status: :unprocessable_entity
        end
      end

      # DELETE /offers/1
      def destroy
        @offer.destroy
      end

      private
      def offers_join_response(requested_entity, id)
        sql_join = "SELECT poffer.*,
            puser2.name as \"applicant_name\" , puser.name as \"owner_name\",
            puser2.email as \"applicant_email\" , puser.email as \"owner_email\",
            pproducts.name as \"owner_product_name\", pproducts2.name as \"applicant_product_name\",
            pproducts.description as \"owner_product_description\", pproducts2.description as \"applicant_product_description\",
            pproducts.shipping as \"owner_product_shipping\", pproducts2.shipping as \"applicant_product_shipping\",
            pproducts.price as \"owner_product_price\", pproducts2.price as \"applicant_product_price\",
            psizes.name as \"owner_product_size\", psizes2.name as \"applicant_product_size\"
              FROM public.offers as poffer,
               public.users as puser, public.users as puser2,
               public.products as pproducts, public.products as pproducts2,
               public.sizes as psizes, public.sizes as psizes2
            	WHERE poffer.owner = puser.id and poffer.applicant = puser2.id
            	and poffer.owner_product = pproducts.id and poffer.applicant_product = pproducts2.id
            	and pproducts.size_id  = psizes.id and pproducts2.size_id  = psizes2.id
            	and poffer.#{requested_entity} = #{id};"
        @result = ActiveRecord::Base.connection.execute(sql_join)
        return @result
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_offer
        @offer = Offer.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def offer_params
        params.require(:offer).permit(:applicant, :applicant_product, :owner, :owner_product, :deal, :cancelled)
      end
    end
  end
end
