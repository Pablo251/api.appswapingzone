module Api
  module V1
    class ProductsController < ApplicationController
      before_action :set_product, only: [:show, :update, :destroy]
      rescue_from ActiveRecord::RecordNotFound, with: :loose_record
      before_action :authenticate

      # GET /products
      def index
        @products = Product.where(status: "deploy").order('created_at DESC').all
          if params[:mine]
            @products = Product.where(user_id: @current_user.id).all
            render json: serialize(@products), status: :ok
          elsif params[:menu]
            @products = Product.where(sponsored: true).all
            # render json: serialize(@products), status: :ok
            render json: serialize(@products), status: :ok
          else
            # render json: serialize(@products.as_json({:include => [:user, :category, :size, :pictures]}))
            # render json: serialize(@products.as_json(:only => [:email, :name]) =>
            render json: serialize(@products.as_json(
            :include => [
              :category,
              :size,
              :user => {:only => [:id, :name, :email, :phone_number]}
              ])), status: :ok
          end
      end

      def order_by_type
        begin
          @products = Product.where(status: "deploy").where(sale_status: params[:param]).order('created_at DESC').all
          # sale_status: params[:param]
          render json: serialize(@products), status: :ok
        rescue ActiveRecord::StatementInvalid
          render json: {:error => "Invalid access"}, status: :unprocessable_entity
        end
      end

      # GET /products/1
      def show
        # render json: serialize(@product.as_json({:include => [:user, :category, :size]}))
        render json: serialize(@product.as_json(
        :include => [
          :category,
          :size,
          :pictures,
          :user => {:only => [:id, :name, :email, :phone_number]}
          ]))
      end

      # POST /products
      def create
        @product = Product.new(product_params)
        @product.status = "pending"
        if @product.save
          render json: serialize(@product), status: :created
        else
          render json: @product.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /products/1
      def update
        if @product.update(product_params)
          render json: serialize(@product), status: :ok
        else
          render json: @product.errors, status: :unprocessable_entity
        end
      end

      # DELETE /products/1
      def destroy
        # Create a query to select the pictures that are associated with the current product
        sql_select = "SELECT picture_id FROM public.pictures_products where product_id=#{@product.id}"
        # Execute the select
        selected_pictures = ActiveRecord::Base.connection.execute(sql_select)
        # Create a query to delete the selected pictures
        # Bucle to delete the registres
        sql_delete = ""
        selected_pictures.each { |a|
          sql_delete = "DELETE FROM public.pictures WHERE id=#{a['picture_id']};"
          deleted_pictures = ActiveRecord::Base.connection.execute(sql_delete)
        }
        # render json: selected_pictures
        # render json: {:ms => sql_delete}
        @product.destroy
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_product
        @product = Product.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def product_params
        params.require(:product).permit(:user_id, :category_id, :size_id, :sponsored, :soponsor_deadline, :pub_date, :name, :description, :status, :price, :location,
        :active, :shipping, :shipping_price, :sale_status)
      end
    end
  end
end
