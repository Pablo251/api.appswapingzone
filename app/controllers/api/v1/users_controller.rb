module Api
  module V1
    class UsersController < ApplicationController
      before_action :set_user, only: [:show, :update, :destroy]
      rescue_from ActiveRecord::RecordNotFound, with: :loose_record
      before_action :authenticate, only: [:index, :show, :update, :destroy]

      # GET /users
      def index
        if @current_user.admin == true
          # Problem with the offers entity
          render json: serialize(@users.as_json({:include => [:products]}))
        else
          render json: {:error => "Unauthorized"}, status: :unauthorized
        end
        @users = User.all
        # render json: {:ms => @current_user}
      end

      # GET /users/1
      def show
        render json: serialize(@user.as_json({:include => [:products]}))
      end

      # POST /users
      def create
        @user = User.new(user_params)
        # Role attributes restrinction
        @user['admin'] = 0
        @user['verifier'] = 0
        @user.password = params[:password]
        if @user.save
          NotifierMailer.sample_email(@user).deliver
          render json: serialize(@user), status: :created
        else
          render json: @user.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /users/1
      def update
        if @user.update(user_params)
          render json: serialize(@user), status: :ok
        else
          render json: @user.errors, status: :unprocessable_entity
        end
      end

      # DELETE /users/1
      def destroy
        @user.destroy
      end

      private
      # Use callbacks to share common setup or constraints between actions.
      def set_user
        @user = User.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def user_params
        params.require(:user).permit(:name, :facebook_id, :phone_number, :apitoken,
        :password, :email, :birt_date, :admin, :verifier, :address)
      end
    end
  end
end
