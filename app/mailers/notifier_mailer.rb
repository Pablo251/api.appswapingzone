class NotifierMailer < ApplicationMailer
  default from: "non-swapingking@gmail.com"

  def sample_email(user)
    @user = user
    mail(to: @user.email, subject: 'New account created')
  end

  def offer_notify(offer)
    @offer = offer
    mail(to: @offer['owner_email'], subject: "New offer received for #{@offer['owner_product_name']}")
  end

  def sale_notify(sale, owner, applicant)
    @sale = sale
    @owner = owner
    @applicant = applicant
    mail(to: @owner['email'], subject: "Hi #{@owner['name']} your product #{@sale['product']['name']} was purchased")
  end
end
