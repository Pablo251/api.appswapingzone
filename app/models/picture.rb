class Picture < ApplicationRecord
  validates_presence_of :alias, :path_url

  # Pivot Picture_Produts reference | Product
  has_and_belongs_to_many :products
end
