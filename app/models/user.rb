require 'bcrypt'
class User < ApplicationRecord
  # Include BCrypt
  include BCrypt

  # Validations
  validates_presence_of :name, :password, :email, :birt_date
  validates :name, length: {maximum: 120}
  validates :phone_number, length: {maximum: 120}
  validates :email, uniqueness: true
  validates :facebook_id, uniqueness: true
  validates :apitoken, uniqueness: true

  # model relationships
  has_many :offers
  has_many :sales
  has_many :products

  # Controller accesor
  def self.call_keygen
    User.new.instance_eval do
      return generate_api_key
    end
  end

  # Assign an API key on create
  before_create do |user|
    user.apitoken = user.generate_api_key
  end

  # Generate a unique API key
  def generate_api_key
    loop do
      token = SecureRandom.base64.tr('+/=', 'Qrt')
      break token unless User.exists?(apitoken: token)
    end
  end

  # Password hash
  def password
     @password ||= Password.new(password_hash)
  end

  def password=(new_password)
     @password = Password.create(new_password)
     self.password_hash = @password
  end

end
