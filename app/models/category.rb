class Category < ApplicationRecord
  validates :name, presence: true, length: { maximum: 35 }
  has_many :size
  has_many :product
end
