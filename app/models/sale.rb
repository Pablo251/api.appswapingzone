class Sale < ApplicationRecord
  # model validations
  validates_presence_of :user_id, :product_id, :note
  # Model relationships
  belongs_to :user
  belongs_to :product
end
