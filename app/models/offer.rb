class Offer < ApplicationRecord
  # model validations
  validates_presence_of :applicant, :applicant_product, :owner, :owner_product
  # Model relationships
  belongs_to :user, :foreign_key => 'applicant'
  belongs_to :user, :foreign_key => 'owner'
  belongs_to :product, :foreign_key => 'applicant_product'
  belongs_to :product, :foreign_key => 'owner_product'
end
