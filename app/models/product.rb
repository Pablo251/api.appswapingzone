class Product < ApplicationRecord
# Fields validation
  validates_presence_of :category_id, :size_id, :soponsor_deadline,
    :pub_date, :name, :description, :status, :price, :location,
    :shipping, :shipping_price, :user_id

  # validates :user_id, presence: true, numericality: { only_integer: true }
  validates :category_id, numericality: { only_integer: true } #Only Integer
  validates :size_id, numericality: { only_integer: true } #Only Integer
  validates :name, length: { maximum: 50 } #maximum characters quantity is 50
  validates :status, length: { maximum: 12 } #maximum characters quantity is 12
  validates :price, numericality: true #Only numbers is acepted
  validates :shipping, length: { maximum: 12 } #maximum characters quantity is 12
  validates :shipping_price, numericality: true #Only numbers is acepted

  # categories association
  belongs_to :category
  # sizes association
  belongs_to :size
  # Pivot Picture_Produts reference | Picture
  has_and_belongs_to_many :pictures
  # User association
  belongs_to :user
  # Offers association
  has_many :offers
  # Sales association
  has_many :sales
end
