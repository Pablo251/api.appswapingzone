require 'test_helper'

class SizesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @size = sizes(:one)
  end

  test "should get index" do
    get sizes_url, as: :json
    assert_response :success
  end

  test "should create size" do
    assert_difference('Size.count') do
      post sizes_url, params: { size: { category_id: @size.category_id, name: @size.name } }, as: :json
    end

    assert_response 201
  end

  test "should show size" do
    get size_url(@size), as: :json
    assert_response :success
  end

  test "should update size" do
    patch size_url(@size), params: { size: { category_id: @size.category_id, name: @size.name } }, as: :json
    assert_response 200
  end

  test "should destroy size" do
    assert_difference('Size.count', -1) do
      delete size_url(@size), as: :json
    end

    assert_response 204
  end
end
