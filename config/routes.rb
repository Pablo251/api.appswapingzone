Rails.application.routes.draw do
  namespace :api, path: '/', defaults: {format: 'json'} do
    namespace :v1 do
      resources :pictures
      resources :users
      resources :offers
      resources :sales
      resources :products
      resources :sizes
      resources :categories
      post 'login' => 'auths#login'
      delete 'logout/:id' => 'auths#logout'
      post 'example' => 'auths#example'
      post 'facebook_login' => 'auths#facebook_login'
      get 'offers/swap/:id' => 'offers#accept_swap'
      get 'products/sort/:param' => 'products#order_by_type'
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
