# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161127102716) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "offers", force: :cascade do |t|
    t.integer  "applicant"
    t.integer  "applicant_product"
    t.integer  "owner"
    t.integer  "owner_product"
    t.boolean  "deal"
    t.boolean  "cancelled"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "pictures", force: :cascade do |t|
    t.string   "alias"
    t.string   "path_url"
    t.text     "based"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pictures_products", id: false, force: :cascade do |t|
    t.integer "product_id", null: false
    t.integer "picture_id", null: false
    t.index ["picture_id"], name: "index_pictures_products_on_picture_id", using: :btree
    t.index ["product_id"], name: "index_pictures_products_on_product_id", using: :btree
  end

  create_table "products", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "category_id"
    t.integer  "size_id"
    t.boolean  "sponsored"
    t.datetime "soponsor_deadline"
    t.datetime "pub_date"
    t.string   "name"
    t.text     "description"
    t.string   "status"
    t.decimal  "price"
    t.text     "location"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.boolean  "active"
    t.string   "shipping"
    t.decimal  "shipping_price"
    t.boolean  "sale_status"
  end

  create_table "sales", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "product_id"
    t.text     "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sizes", force: :cascade do |t|
    t.integer  "category_id"
    t.string   "name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "facebook_id"
    t.string   "phone_number"
    t.string   "apitoken"
    t.string   "password_hash"
    t.string   "email"
    t.datetime "birt_date"
    t.boolean  "admin"
    t.boolean  "verifier"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.text     "address"
    t.index ["apitoken"], name: "index_users_on_apitoken", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["facebook_id"], name: "index_users_on_facebook_id", unique: true, using: :btree
  end

  add_foreign_key "offers", "products", column: "applicant_product", on_delete: :cascade
  add_foreign_key "offers", "products", column: "owner_product", on_delete: :cascade
  add_foreign_key "offers", "users", column: "applicant", on_delete: :cascade
  add_foreign_key "offers", "users", column: "owner", on_delete: :cascade
  add_foreign_key "pictures_products", "pictures", on_delete: :cascade
  add_foreign_key "pictures_products", "products", on_delete: :cascade
  add_foreign_key "products", "categories", on_delete: :cascade
  add_foreign_key "products", "sizes", on_delete: :cascade
  add_foreign_key "sales", "products", on_delete: :cascade
  add_foreign_key "sales", "users", on_delete: :cascade
  add_foreign_key "sizes", "categories", on_delete: :cascade
end
