class CreatePivotProductAndPicture < ActiveRecord::Migration[5.0]
  def change
    create_join_table :products, :pictures do |t|
      t.index :product_id
      t.index :picture_id
    end
  end
end
