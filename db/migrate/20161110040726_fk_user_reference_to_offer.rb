class FkUserReferenceToOffer < ActiveRecord::Migration[5.0]
  def change
    add_foreign_key :offers, :users, column: :applicant, primary_key: "id", on_delete: :cascade
    add_foreign_key :offers, :users, column: :owner, primary_key: "id", on_delete: :cascade
  end
end
