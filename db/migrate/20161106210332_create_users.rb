class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :facebook_id
      t.string :phone_number
      t.string :apitoken
      t.string :password
      t.string :email
      t.datetime :birt_date
      t.boolean :admin
      t.boolean :verifier

      t.timestamps
    end
  end
end
