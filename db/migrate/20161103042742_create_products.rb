class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.integer :user_id
      t.integer :category_id
      t.integer :sizze_id
      t.boolean :sponsored
      t.datetime :soponsor_deadline
      t.datetime :pub_date
      t.string :name
      t.text :description
      t.string :status
      t.decimal :price
      t.text :location

      t.timestamps
    end
  end
end
