class ForeignKeyProductsPictures < ActiveRecord::Migration[5.0]
  def change
    add_foreign_key :pictures_products, :products, column: :product_id, primary_key: "id", on_delete: :cascade
    add_foreign_key :pictures_products, :pictures, column: :picture_id, primary_key: "id", on_delete: :cascade
  end
end
