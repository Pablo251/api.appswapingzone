class AddColumnsProducts < ActiveRecord::Migration[5.0]
  def change
    change_table :products do |t|
      t.boolean :active
      t.string  :shipping
      t.decimal :shipping_price
      t.boolean :sale_status
    end
  end
end
