class CreatePictures < ActiveRecord::Migration[5.0]
  def change
    create_table :pictures do |t|
      t.string :alias
      t.string :path_url
      t.text :based

      t.timestamps
    end
  end
end
