class ApiKeyUniqueUser < ActiveRecord::Migration[5.0]
  def change
    add_index :users, :apitoken, unique: true
  end
end
