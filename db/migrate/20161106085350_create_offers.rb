class CreateOffers < ActiveRecord::Migration[5.0]
  def change
    create_table :offers do |t|
      t.integer :applicant
      t.integer :applicant_product
      t.integer :owner
      t.integer :owner_product
      t.boolean :deal
      t.boolean :cancelled

      t.timestamps
    end
  end
end
