class AddOfferFk3 < ActiveRecord::Migration[5.0]
  def change
    add_foreign_key :offers, :products, column: :applicant_product, primary_key: "id", on_delete: :cascade
    add_foreign_key :offers, :products, column: :owner_product, primary_key: "id", on_delete: :cascade
  end
end
