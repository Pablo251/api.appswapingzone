class SalesFkReferences < ActiveRecord::Migration[5.0]
  def change
    add_foreign_key :sales, :users, column: :user_id, primary_key: "id", on_delete: :cascade
    add_foreign_key :sales, :products, column: :product_id, primary_key: "id", on_delete: :cascade
  end
end
