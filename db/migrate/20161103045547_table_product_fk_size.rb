class TableProductFkSize < ActiveRecord::Migration[5.0]
  def change
    add_foreign_key :products, :sizes, column: :sizze_id, primary_key: "id", on_delete: :cascade
  end
end
