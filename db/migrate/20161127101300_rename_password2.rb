class RenamePassword2 < ActiveRecord::Migration[5.0]
  def change
    change_table :users do |t|
      t.rename :password_hash, :password
    end
  end
end
